SNAKE
====

This game requires the library [pygame](http://www.pygame.org/download.shtml) to work.

You can download it with your favourite packages downloader (apt-get,yum,pacman,...) or
with pip

To launch the game, just enter the following command

    python snake.py
