#Unit size (food, one part of Snake's body, ...)
part_size = 20

#Size of Snake when you start the game
initial_size = 5

#Size of the screen
#/!\ IF YOUR SCREEN SIZE IS TO SMALL YOU MAY ENCOUNTER TEXT DISPLAY ISSUES
screen_size = (800,800)

#Size of the barriers
offset = 20

#Seed of the game
game_speed = 20

#Size of the score's display
score_offset = 30
