#!/usr/bin/env python

import pygame
from pygame.locals import *
from random import randint
from math import floor
import sys
import os.path

path = '/home/HamtaroWarrior/Documents/snake-pygame'

os.chdir(path)
sys.path.insert(0, path)

from config import *

R,L,D,U = [(1,0),(-1,0),(0,1),(0,-1)]

class snake:
    def __init__(self):
        pygame.font.init()
        self.action = ''
        self.font = pygame.font.Font("assets/space_invaders.ttf", score_offset)
        self.body = [(int(floor(screen_size[0]/2)), int(floor(screen_size[1]/2)))]
        self.eated = []
        self.growth_timer = []
        self.old_direction = U
        self.direction = U
        self.score = 0
        self.best_score = 0
        self.best_score_is_written = False
        if (os.path.exists('best_score')):
            bsfile = open('best_score', 'r')
            self.best_score = int(bsfile.read())
            bsfile.close
        self.screen = pygame.display.set_mode((screen_size[0]+offset,screen_size[1]+offset + score_offset))
        self.head_sprite = pygame.image.load("assets/head.png").convert()
        for e in range(initial_size):
            self.body.append((self.body[0][0], self.body[0][1] + (e+1)*part_size))
        self.pop_food()

    def key_listener(self):
        key = pygame.key.get_pressed()
        if key[K_d] or key[K_s] or key[K_a] or key[K_w]:
            self.update_player_direction(key)
        if key[K_ESCAPE]:
            self.write_best_score()
            sys.exit()
        if key[K_p]:
            self.pause_game()
        if key[K_r]:
            self.__init__()
        pygame.event.clear()

    def pause_game(self):
        key = pygame.key.get_pressed()
        self.action = 'p'
        while key[K_p]:
            self.game_display()
            key = pygame.key.get_pressed()
            pygame.event.clear()
        while not(key[K_p]):
            self.game_display()
            key = pygame.key.get_pressed()
            pygame.event.clear()
        while key[K_p]:
            self.game_display()
            key = pygame.key.get_pressed()
            pygame.event.clear()
        self.action = ''

    def update_player_direction(self, key):
        if key[K_d] and self.direction != L and self.old_direction != L:
            self.direction = R
        elif key[K_a] and self.direction != R and self.old_direction != R:
            self.direction = L
        elif key[K_w] and self.direction != D and self.old_direction != D:
            self.direction = U
        elif key[K_s] and self.direction != U and self.old_direction != U:
            self.direction = D
   
    def update_player_position(self):
        self.body.insert(0,tuple([self.body[0][x] + self.direction[x]*part_size for x in range(2)]))
        self.body.pop()

    def collides(self):
        on_wall = self.body[0][0] < 0 or self.body[0][1] < 0 or self.body[0][0] > screen_size[0]- part_size - (offset/2) or self.body[0][1] > screen_size[1] - part_size - (offset/2)
        on_itself = self.body[0] in self.body[1:]
        return (on_wall or on_itself) 

    def takes_all_place(self):
        return len(self.body) == ((screen_size[0] * screen_size[1])/part_size)

    def pop_food(self):
        pop_pos = self.body[0]
        while (pop_pos in self.body):
            pop_pos = tuple([x*part_size for x in (randint(0,((screen_size[0]-offset/2)/part_size)-1), randint(0,((screen_size[1]-offset/2)/part_size)-1))])
        self.food = pop_pos

    def eat_food(self):
        self.score += 10
        if (self.score > self.best_score):
            self.best_score = self.score
        self.eated.append(self.food)
        self.growth_timer.append(len(self.body))
        self.pop_food()

    def grow(self):
        self.growth_timer = [timer-1 for timer in self.growth_timer]
        if (self.growth_timer[0] == 0):
            self.growth_timer.pop(0)
            self.body.append(self.eated.pop(0))

    def display_body(self):
        pygame.draw.rect(self.screen,(255,0,0),pygame.Rect(self.body[0][0]+offset, self.body[0][1]+offset, part_size, part_size))
        for x,y in self.body[1:]:
            pygame.draw.rect(self.screen,(0,255,0),pygame.Rect(x+offset, y+offset, part_size, part_size))

    def display_food(self):
        pygame.draw.rect(self.screen,(255,255,255),pygame.Rect(self.food[0]+offset,self.food[1]+offset, part_size, part_size))

    def display_borders(self):
        pygame.draw.rect(self.screen,(128,128,128),pygame.Rect(0,0, screen_size[0], offset))
        pygame.draw.rect(self.screen,(128,128,128),pygame.Rect(0,0, offset, screen_size[1]))
        pygame.draw.rect(self.screen,(128,128,128),pygame.Rect(0, screen_size[1], screen_size[0]+offset, offset))
        pygame.draw.rect(self.screen,(128,128,128),pygame.Rect(screen_size[0], 0, offset, screen_size[1]+offset))

    def display_score(self):
        self.screen.blit(self.font.render("Score:{}".format(self.score), -1, (255,255,255)), (10,screen_size[0]+offset))
        self.screen.blit(self.font.render("Max Score:{}".format(self.best_score), -1, (255,255,255)), (460,screen_size[0]+offset))

    def game_display(self):    
        self.display_borders()
        self.display_food()
        self.display_body()
        self.display_score()
        if self.action == 'w':
            self.screen.blit(pygame.font.Font("assets/space_invaders.ttf", 100).render("You Win!", -1, (52,255,0)), (100, 200))
        if self.action == 'l': 
            self.screen.blit(pygame.font.Font("assets/space_invaders.ttf", 100).render("You Lose!", -1, (52,255,0)), (100, 200))
        if self.action == 'p':
            self.screen.blit(pygame.font.Font("assets/space_invaders.ttf", 100).render("Paused", -1, (52,255,0)), (200, 200))
            self.screen.blit(pygame.font.Font("assets/space_invaders.ttf", 35).render("Press P to resume", -1, (255,255,255)), (200, 400))
        pygame.display.flip()


    def write_best_score(self):
        if (not(self.best_score_is_written)):
            bsfile = open('best_score', 'w')
            bsfile.write(str(self.best_score))
            bsfile.close()
            self.best_score_is_written = True


    def run(self):
        clock = pygame.time.Clock()
        it = False
        while(True):
            clock.tick(game_speed)
            self.screen.fill((0,0,0))
            if self.takes_all_place():
                self.action = 'w'
                self.write_best_score()
            elif self.collides():
                self.action = 'l'
                self.write_best_score()
            else:
                if it: 
                    self.old_direction = self.direction
                    self.update_player_position()
                    if self.eated:
                        self.grow()
            self.key_listener()
            self.game_display()
            if self.food == self.body[0]:
                self.eat_food()
            it = not(it)

snake().run()
